I found several old chips, including some PALs, in a bag and was wondering
what the PALs were programmed to do as the original purpose was long forgotten. 
(post at https://www.reddit.com/r/AskElectronics/comments/g61fwd/cleaning_up_and_i_found_an_anti_static_bag/ )

This sketch simply toggles all bits of the chip's inputs. You can have LEDs on
the outputs or have the Arduino itself map with its IO pins.

Will probably expand this with some logic level shifters to maximize the number 
of pins it can toggle and watch.

Tested with TIBPAL16L8-25CN and TIBPAL16R8-25CN chips.

It's a real one-trick-pony but may be of use to somebody out there.