
/* 
 *  PAL Map
 *  tested with: 
 *  TIBPAL16L8-25CN
 *  TIBPAL16R8-25CN
 *  v 0.2  grub@grub.net 20200422
 *  To Do: 
 *  - expand IO ports through shift registers 
 *  - do proper logic mapping and export	
 */

// TIBPAL16L8-25CN has 10 default input pins. 
// Am assuming IO were all O in my case.
int startPin = 2;
int endPin = 11;
int numPins = (endPin-startPin+1);
int palPins[] = {2, 3, 4, 5, 6, 7, 8, 9, 10, 11};

int cntMax = pow(2,numPins);
int timer1 = 100; // delay in ms
int ledTog = 0;   // LED toggle state

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  Serial.begin(9600);
//  Serial.begin(230400); // zoom.
  Serial.println("\nQuick & Dirty PAL Mapper");
  Serial.println("------------------------\n");
  
  for (int a = startPin; a <= endPin; a++) {
    pinMode(a, OUTPUT);
  }
}

void loop() {
  
  for (int a = 0; a < cntMax; a++) {

  if ( ledTog == 0 ) { 
    ledTog = 1;
    digitalWrite(LED_BUILTIN, HIGH);
  } else {
    ledTog = 0;
    digitalWrite(LED_BUILTIN, LOW);
  } 

  byte pinLow = lowByte(a);
  byte pinHigh = highByte(a);
  
  digitalWrite(2, (pinLow & 1));
  digitalWrite(3, (pinLow & 2));
  digitalWrite(4, (pinLow & 4));
  digitalWrite(5, (pinLow & 8));
  digitalWrite(6, (pinLow & 16));
  digitalWrite(7, (pinLow & 32));
  digitalWrite(8, (pinLow & 64));
  digitalWrite(9, (pinLow & 128));
  digitalWrite(10, (pinHigh & 1));
  digitalWrite(11, (pinHigh & 2));


// Prints current pinLow and pinHigh data. 
// No leading zeroes, mainly for debugging.
  Serial.print(a);
  Serial.print(" --- ");
  Serial.print(pinLow, BIN);
  Serial.println(pinHigh, BIN);

  delay(timer1);
  
  }
}
  
